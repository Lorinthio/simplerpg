package me.Lorinth.SimpleRpg.enums;

/**
 * Created by Ben on 1/14/2017.
 */
public enum Statistic {
    Strength, Constitution, Dexterity, Wisdom, Intelligence
}
