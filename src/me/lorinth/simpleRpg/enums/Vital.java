package me.Lorinth.SimpleRpg.enums;

/**
 * Created by Ben on 1/14/2017.
 */
public enum Vital {
    Health, MaxHealth, Energy, MaxEnergy
}
