package me.Lorinth.SimpleRpg.loot;

import me.Lorinth.SimpleRpg.common.EquipmentHelper;
import me.Lorinth.SimpleRpg.enums.Rarity;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Ben on 1/14/2017.
 */
public class ItemGenerator {

    private Random random = new Random();

    public ArrayList<ItemStack> Tier1;
    public ArrayList<ItemStack> Tier2;
    public ArrayList<ItemStack> Tier3;
    public ArrayList<ItemStack> Tier4;
    public ArrayList<ItemStack> Tier5;
    public ArrayList<ItemStack> Tier6;
    public ArrayList<ItemStack> Tier7;
    public ArrayList<ItemStack> Tier8;
    public ArrayList<ItemStack> Tier9;
    public ArrayList<ItemStack> Tier10;

    public ItemGenerator(){
        MakeItems();
    }

    private void MakeItems(){
        MakeTier1();
    }

    private void MakeTier1(){
        //Weapons

        Tier1.add(MakeBaseWeapon(Material.WOOD_SWORD, (short) 0, "Wooden Scabbard", 1, 4, new String[]{
                "You couldn't find",
                "a real sword, but",
                "this will do..."}));
        Tier1.add(MakeBaseWeapon(Material.WOOD_AXE, (short) 0, "Wooden Hatchet", 1, 5, new String[]{
                "How is wood",
                "supposed to chop",
                "itself???"
        }));
        Tier1.add(MakeBaseWeapon(Material.BOW, (short)0, "Cracked Short Bow", 1, 5, new String[]{
                "This bow doesn't",
                "seem like it will",
                "hold very long..."
        }));
        Tier1.add(MakeBaseWeapon(Material.STICK, (short) 0, "Broken Wand", 1, 4, new String[]{
                "This stick seems",
                "to lack any power.",
                "I can still beat",
                "monsters with it!"
        }));
    }

    private ItemStack MakeBaseWeapon(Material type, short data, String name, int tier, int damage, String[] description){
        ItemStack item = new ItemStack(type, 1, data);
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(name);


        //name
        //tier
        //rarity (calculated later)
        //damage
        //bonuses
        //description

        ArrayList<String> lore = new ArrayList<>();
        lore.add(ChatColor.GOLD + "Tier " + ConvertTierToString(tier));
        lore.add("" + damage);

        lore.add("");
        for(String line : description){
            lore.add(line);
        }

        item.setItemMeta(meta);
        return item;
    }

    public ItemStack GenerateItem(int level){
        int tier = calculateTierForLevel(level);
        ArrayList<ItemStack> baseItems = getSelectionForTier(tier);

        ItemStack item = baseItems.get(random.nextInt(baseItems.size()));
        item = randomizeItem(item, tier);
        return item;
    }

    private ItemStack randomizeItem(ItemStack item, int tier){
        ItemStack newItem = item.clone();

        Rarity rarity = rollRarity();
        String rarityColorTag = "";
        String rarityString = "";
        int minBonuses = 0;
        int maxBonuses = 0;
        int maxDamage = 0;

        switch(rarity){
            case Common:
                rarityColorTag = ChatColor.DARK_GRAY + "";
                rarityString = rarityColorTag + rarity.name();
                minBonuses = 0;
                maxBonuses = 0;
                maxDamage = tier;
                break;
            case Uncommon:
                rarityColorTag = ChatColor.AQUA + "";
                rarityString = rarityColorTag + rarity.name();
                minBonuses = 1;
                maxBonuses = 1;
                maxDamage = tier;
                break;
            case Rare:
                rarityColorTag = ChatColor.GREEN + "";
                rarityString = rarityColorTag + rarity.name();
                minBonuses = 1;
                maxBonuses = 1;
                maxDamage = tier * 2;
                break;
            case Epic:
                rarityColorTag = ChatColor.DARK_RED + "";
                rarityString = rarityColorTag + rarity.name();
                minBonuses = 1;
                maxBonuses = 2;
                maxDamage = tier * 2;
                break;
            case Legendary:
                rarityColorTag = ChatColor.DARK_PURPLE + "";
                rarityString = rarityColorTag + rarity.name();
                minBonuses = 2;
                maxBonuses = 3;
                maxDamage = tier * 3;
                break;
            case Godly:
                rarityColorTag = ChatColor.GOLD + "";
                rarityString = rarityColorTag + rarity.name();
                minBonuses = 3;
                maxBonuses = 4;
                maxDamage = tier * 3;
                break;
        }

        int bonusCount = random.nextInt(maxBonuses - minBonuses) + minBonuses;
        int addDamage = random.nextInt(maxDamage - tier) + (tier-1);

        ItemMeta meta = newItem.getItemMeta();
        ArrayList<String> lore = (ArrayList<String>) meta.getLore();

        //Get base damage from 2nd line of lore
        int baseDamage = Integer.parseInt(lore.get(1));
        //Add our random damage addition
        baseDamage += addDamage;

        if(EquipmentHelper.IsMeleeWeapon(item)){
            lore.set(1, ChatColor.RED + "Damage" + ChatColor.GRAY + " : " + ChatColor.RED + baseDamage);
        }
        if(EquipmentHelper.IsRangedWeapon(item)){
            lore.set(1, ChatColor.GREEN + "Damage" + ChatColor.GRAY + " : " + ChatColor.GREEN + baseDamage);
        }
        if(EquipmentHelper.IsMagicWeapon(item)){
            lore.set(1, ChatColor.LIGHT_PURPLE + "Damage" + ChatColor.GRAY + " : " + ChatColor.LIGHT_PURPLE + baseDamage);
        }
        lore.add(2, rarityString);

        for(int i=0; i<bonusCount; i++){
            addBonus(tier, lore);
        }

        newItem.setItemMeta(meta);
        return newItem;
    }

    private void addBonus(int tier, ArrayList<String> lore){

    }

    private Rarity rollRarity(){
        int roll = random.nextInt(1000);

        int current = 0;
        current += 1; //0.1%
        if(roll < current ){
            return Rarity.Godly;
        }

        current += 5; // 0.5%
        if(roll < current){
            return Rarity.Legendary;
        }

        current += 24; //2.4%
        if(roll < current){
            return Rarity.Epic;
        }

        current += 170; //17%
        if(roll < current){
            return Rarity.Rare;
        }

        current += 300; //30%
        if(roll < current){
            return Rarity.Uncommon;
        }

        return Rarity.Common;
    }

    private int calculateTierForLevel(int level){
        level -= level%10;  //remove remainder
        level = level / 10; //divide by 10
        level += 1;         //add one to tier

        return level;
    }

    private ArrayList<ItemStack> getSelectionForTier(int tier){
        if(tier == 1)
            return Tier1;
        if(tier == 2)
            return Tier2;
        if(tier == 3)
            return Tier3;
        if(tier == 4)
            return Tier4;
        if(tier == 5)
            return Tier5;
        if(tier == 6)
            return Tier6;
        if(tier == 7)
            return Tier7;
        if(tier == 8)
            return Tier8;
        if(tier == 9)
            return Tier9;
        if(tier == 10)
            return Tier10;

        return Tier1;
    }

    private String ConvertTierToString(int tier){
        if(tier == 1)
            return "I";
        if(tier == 2)
            return "II";
        if(tier == 3)
            return "III";
        if(tier == 4)
            return "IV";
        if(tier == 5)
            return "V";
        if(tier == 6)
            return "VI";
        if(tier == 7)
            return "VII";
        if(tier == 8)
            return "VIII";
        if(tier == 9)
            return "IX";
        if(tier == 10)
            return "X";
        return "Tier Not Defined";
    }

}
