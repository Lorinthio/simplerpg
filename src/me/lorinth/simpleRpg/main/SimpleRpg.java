package me.Lorinth.SimpleRpg.main;

import me.Lorinth.SimpleRpg.listeners.CombatListener;
import me.Lorinth.SimpleRpg.listeners.ExperienceListener;
import me.Lorinth.SimpleRpg.listeners.HeroDataListener;
import me.Lorinth.SimpleRpg.listeners.SkillListener;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Created by Ben on 1/10/2017.
 */
public class SimpleRpg extends JavaPlugin{

    private CombatListener combatListener;
    private ExperienceListener experienceListener;
    private HeroDataListener heroDataListener;
    private SkillListener skillListener;
    private HeroManager heroManager;
    private static SimpleRpg instance;

    @Override
    public void onEnable(){
        instance = this;
        heroManager = new HeroManager();
        registerEvents();
    }

    @Override
    public void onDisable(){
        HeroManager.Stop();
    }

    public static SimpleRpg GetInstance(){
        return instance;
    }

    private void registerEvents(){
        combatListener = new CombatListener();
        experienceListener = new ExperienceListener();
        heroDataListener = new HeroDataListener();
        skillListener = new SkillListener();

        Bukkit.getPluginManager().registerEvents(combatListener, this);
        Bukkit.getPluginManager().registerEvents(experienceListener, this);
        Bukkit.getPluginManager().registerEvents(heroDataListener, this);
        Bukkit.getPluginManager().registerEvents(skillListener, this);
    }

    @EventHandler
    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args)
    {
        if(sender instanceof Player){
            Player p = (Player) sender;
            if(skillListener.handleCommand(p, cmd, commandLabel, args))
                return false;
            if(heroDataListener.handleCommand(p, cmd, commandLabel, args))
                return false;
        }
        return false;
    }

}
