package me.Lorinth.SimpleRpg.main;

import me.Lorinth.SimpleRpg.character.Hero;
import me.Lorinth.SimpleRpg.character.Skills;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import java.io.File;
import java.util.HashMap;

/**
 * Created by Ben on 1/10/2017.
 */
public class HeroManager {

    private static HeroManager instance;
    private static HashMap<Player, Hero> Heroes;

    public static void LoadHeroForPlayer(Player p){
        File f = new File(SimpleRpg.GetInstance().getDataFolder() + File.separator + "Heroes", p.getUniqueId().toString() + ".yml");
        Hero h = new Hero(p);

        if(f.exists()){
            System.out.println("Loading Data for " + p.getDisplayName());
            YamlConfiguration yml = YamlConfiguration.loadConfiguration(f);
            h.Load(yml);
        }
        else{
            System.out.println("Creating new Data for " + p.getDisplayName());
            h.Create();
        }

        Heroes.put(p, h);
    }

    public static void SaveHeroForPlayer(Player p){
        File f = new File(SimpleRpg.GetInstance().getDataFolder() + File.separator + "Heroes", p.getUniqueId().toString() + ".yml");
        YamlConfiguration yml = YamlConfiguration.loadConfiguration(f);
        Hero h = getHeroByPlayer(p);
        h.Save(yml);

        try{
            yml.save(f);
        }
        catch(Exception e){
            System.out.println(e.getCause() + " when saving, " + p.getDisplayName());
        }
    }

    public HeroManager(){
        instance = this;
        Start();
    }

    private static void Start(){
        Heroes = new HashMap<>();

        for(Player p : Bukkit.getOnlinePlayers()){
            HeroManager.LoadHeroForPlayer(p);
        }
    }

    public static void Stop(){
        for(Player p : Bukkit.getOnlinePlayers()){
            HeroManager.SaveHeroForPlayer(p);
        }
    }

    public static Hero getHeroByPlayer(Player p){
        return Heroes.get(p);
    }

    public static Skills getSkillsByPlayer(Player p){
        Hero h = getHeroByPlayer(p);
        return h.getSkills();
    }

}
