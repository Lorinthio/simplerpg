package me.Lorinth.SimpleRpg.inventory;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;

/**
 * Created by Ben on 1/12/2017.
 */
public class ClassMenu extends InventoryMenu{

    public ClassMenu(){
        super(27, "Select a Class");

        this.setup();
    }

    private void setup(){
        ItemStack Warrior = new ItemStack(Material.IRON_AXE);
        ItemMeta meta = Warrior.getItemMeta();
        meta.setDisplayName(ChatColor.RED + "Warrior");
        meta.setLore(new ArrayList<String>(){{
            add(ChatColor.GRAY + "Role : Melee Damage");
            add(ChatColor.GRAY + "Weapon(s) : " + ChatColor.RED + "Dual Wield Axes");
            add(ChatColor.GRAY + "Armor : " + ChatColor.DARK_GRAY + "Iron");
            add(ChatColor.GRAY + "Primary Attribute : " + ChatColor.RED + "Strength");
            add(ChatColor.GOLD + "Skills : ");
            add(ChatColor.GRAY + " • Whirlwind (Small AOE)");
            add(ChatColor.GRAY + " • Crush (Stun)");
            add(ChatColor.GRAY + " • Adrenaline (Shield + Damage Buff)");
            add(ChatColor.GRAY + "Passive");
            add(ChatColor.GRAY + "Enrage : Deal more melee damage");
            add(ChatColor.GRAY + "while engaged in combat");
        }});
        Warrior.setItemMeta(meta);

        ItemStack Paladin = new ItemStack(Material.SHIELD);
        meta = Paladin.getItemMeta();
        meta.setDisplayName(ChatColor.GOLD + "Paladin");
        meta.setLore(new ArrayList<String>(){{
            add(ChatColor.GRAY + "Role : Melee Support");
            add(ChatColor.GRAY + "Weapon(s) : " + ChatColor.DARK_AQUA + "Sword & Shield");
            add(ChatColor.GRAY + "Armor : " + ChatColor.GOLD + "Gold");
            add(ChatColor.GRAY + "Primary Attribute : " + ChatColor.GOLD + "Constitution");
            add(ChatColor.GOLD + "Skills : ");
            add(ChatColor.GRAY + " • Heal (Self/Target Heal)");
            add(ChatColor.GRAY + " • Consecrate (AoE DoT)");
            add(ChatColor.GRAY + " • Charge (Short Dash/Stun)");
            add(ChatColor.GRAY + "Passive");
            add(ChatColor.GRAY + "Sentinel : Take less physical");
            add(ChatColor.GRAY + "damage");
        }});
        Paladin.setItemMeta(meta);

        ItemStack Ranger = new ItemStack(Material.BOW);
        meta = Ranger.getItemMeta();
        meta.setDisplayName(ChatColor.GREEN + "Ranger");
        meta.setLore(new ArrayList<String>(){{
            add(ChatColor.GRAY + "Role : Ranged Damage");
            add(ChatColor.GRAY + "Weapon(s) : " + ChatColor.DARK_GREEN + "Bow");
            add(ChatColor.GRAY + "Armor : Chain");
            add(ChatColor.GRAY + "Primary Attribute : " + ChatColor.GREEN + "Dexterity");
            add(ChatColor.GRAY + "Skills : ");
            add(ChatColor.GRAY + " • Snipe (Long Range Damage)");
            add(ChatColor.GRAY + " • Cripple Shot (Slow)");
            add(ChatColor.GRAY + " • Poison Arrow (Single Target DoT)");
            add(ChatColor.GRAY + "Passive");
            add(ChatColor.GRAY + "True Shot : Your arrows travel");
            add(ChatColor.GRAY + "faster and farther");
        }});
        Ranger.setItemMeta(meta);

        ItemStack Mage = new ItemStack(Material.BLAZE_ROD);
        meta = Mage.getItemMeta();
        meta.setDisplayName(ChatColor.LIGHT_PURPLE + "Mage");
        meta.setLore(new ArrayList<String>(){{
            add(ChatColor.GRAY + "Role : Magic Damage");
            add(ChatColor.GRAY + "Weapon(s) : " + ChatColor.DARK_PURPLE + "Staves");
            add(ChatColor.GRAY + "Armor : Leather");
            add(ChatColor.GRAY + "Primary Attribute : " + ChatColor.LIGHT_PURPLE + "Intelligence");
            add(ChatColor.GRAY + "Skills : ");
            add(ChatColor.GRAY + " • Fireball (Burning)");
            add(ChatColor.GRAY + " • Freeze (Long Range Stun)");
            add(ChatColor.GRAY + " • Blind (Single Target Blind)");
            add(ChatColor.GRAY + "Passive");
            add(ChatColor.GRAY + "Mana Fount : Increase energy");
            add(ChatColor.GRAY + "regeneration");
        }});
        Mage.setItemMeta(meta);

        this.AddItem(Warrior, 10);
        this.AddItem(Paladin, 12);
        this.AddItem(Ranger, 14);
        this.AddItem(Mage, 16);
    }

    public void Open(Player p){
        p.openInventory(this.inventory);
    }

    @Override
    public void OnClick(int slot){

    }

}
