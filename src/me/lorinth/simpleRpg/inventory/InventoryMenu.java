package me.Lorinth.SimpleRpg.inventory;

import org.bukkit.Bukkit;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

/**
 * Created by Ben on 1/12/2017.
 */
public abstract class InventoryMenu {

    Inventory inventory;

    public InventoryMenu(int size, String MenuName){
        this.inventory = Bukkit.createInventory(null, size, MenuName);
    }

    public void AddItem(ItemStack item, int position){
        inventory.setItem(position, item);
    }

    public abstract void OnClick(int slot);

}
