package me.Lorinth.SimpleRpg.listeners;

import me.Lorinth.SimpleRpg.character.Hero;
import me.Lorinth.SimpleRpg.main.HeroManager;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.projectiles.ProjectileSource;

/**
 * Created by Ben on 1/10/2017.
 */
public class CombatListener implements Listener{

    @EventHandler(priority = EventPriority.LOWEST)
    public void EntityDamageEntityEvent(EntityDamageByEntityEvent event){
        if(event.isCancelled())
            return;

        Entity attacker = event.getDamager();
        Entity defender = event.getEntity();

        if(attacker instanceof Projectile){
            //Check who shot the projectile
            ProjectileSource source = ((Projectile) attacker).getShooter();
            if(source instanceof Entity)
                attacker = (Entity) source;
        }

        if(attacker instanceof Player || defender instanceof Player){
            double damage = event.getDamage();

            //Check when player gets hit by entity or player
            if(defender instanceof Player){
                Hero hero = HeroManager.getHeroByPlayer((Player) defender);
                hero.Damage(attacker, damage);
                event.setDamage(0); //nullify normal damage
                return;
            }



            //Check when a player attacks something other than player
            if(attacker instanceof Player && defender instanceof LivingEntity){
                Hero hero = HeroManager.getHeroByPlayer((Player) attacker);
                hero.Attack((LivingEntity) defender);
                event.setDamage(0); //nullify normal damage
                return;
            }
        }
    }
}
