package me.Lorinth.SimpleRpg.listeners;

import me.Lorinth.SimpleRpg.skills.Building;
import me.Lorinth.SimpleRpg.skills.Digging;
import me.Lorinth.SimpleRpg.character.Hero;
import me.Lorinth.SimpleRpg.character.Skills;
import me.Lorinth.SimpleRpg.inventory.ClassMenu;
import me.Lorinth.SimpleRpg.main.HeroManager;
import me.Lorinth.SimpleRpg.skills.Acrobatics;
import me.Lorinth.SimpleRpg.skills.Mining;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffectType;

import java.util.Collection;
import java.util.HashMap;
import java.util.Random;

import static me.Lorinth.SimpleRpg.skills.Mining.GetExperienceForBlock;

/**
 * Created by Ben on 1/10/2017.
 */
public class SkillListener implements Listener {

    //This class handles players earning skill experience as well as skill procs

    private static HashMap<String, Skills.Skill> skillNames;

    public SkillListener(){
        skillNames = new HashMap<>();
        for(Skills.Skill skill : Skills.Skill.values()){
            skillNames.put(skill.name().toLowerCase(), skill);
        }
    }

    final Random random = new Random();

    private boolean isSkillName(String value){
        if(skillNames.keySet().contains(value)){
            return true;
        }
        return false;
    }

    public boolean handleCommand(Player p, Command cmd, String commandLabel, String[] args){
        if(commandLabel.equalsIgnoreCase("skills")) {
            if (args.length == 0) {
                p.sendMessage(ChatColor.GOLD + "" + ChatColor.UNDERLINE + "Skills");
                Hero h = HeroManager.getHeroByPlayer(p);
                h.getSkills().SendSkills(p);
            } else {
                String first = args[0];
                first = first.toLowerCase();

                //If first argument is a skill name, display detailed skill info
                if(isSkillName(first)){
                    Skills.Skill skill = skillNames.get(first);
                    Hero h = HeroManager.getHeroByPlayer(p);
                    h.getSkills().SendDetailedSkillData(skill, p);
                }

                //If first argument is player name, display player info
                else{
                    Player target = Bukkit.getPlayer(first);
                    if (target != null) {
                        Hero h = HeroManager.getHeroByPlayer(target);
                        h.getSkills().SendSkills(p);
                    } else {
                        p.sendMessage(ChatColor.RED + "User not found! User has to be online to be inspected");
                    }
                }
            }
            return true;
        }
        else if(commandLabel.equalsIgnoreCase("classes")){
            ClassMenu menu = new ClassMenu();
            menu.Open(p);
        }
        return false;
    }

    //region Acrobatics
    @EventHandler
    public void CheckDodge(EntityDamageEvent event){
        if(event.isCancelled() || event.getCause() != EntityDamageEvent.DamageCause.ENTITY_ATTACK){
            return;
        }

        Entity entity = event.getEntity();
        if(!(entity instanceof Player))
            return;

        Player p = (Player) entity;
        if(p.getGameMode() == GameMode.SURVIVAL){
            double roll = random.nextDouble() * 1000;
            Skills s = HeroManager.getSkillsByPlayer((Player) entity);
            if(roll < Acrobatics.GetDodgeChance(s)){
                p.sendMessage(ChatColor.GREEN + "***Dodge***");
                p.setNoDamageTicks(20);
                s.AddSkillExperience(Skills.Skill.Acrobatics, Acrobatics.DodgeExpAward);
                event.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void OnFallDamage(EntityDamageEvent event){
        if(event.isCancelled() || event.getCause() != EntityDamageEvent.DamageCause.FALL)
            return;

        Entity entity = event.getEntity();
        if(!(entity instanceof Player))
            return;
        if(((Player)entity).getGameMode() != GameMode.SURVIVAL)
            return;


        Skills s = HeroManager.getSkillsByPlayer((Player) entity);
        double reduction = 0;
        double roll = random.nextDouble() * 1000;

        if(roll < Acrobatics.GetRollChance(s)) {
            reduction = 0.25; //25% reduction

            if (roll < Acrobatics.GetGracefulRollChance(s)) {
                reduction = 0.5; //50% reduction

                if (roll < Acrobatics.GetMasterRollChance(s)) {
                    reduction = 1.0; //100% reduction
                }
            }
        }

        if(reduction > 0){
            Hero h = HeroManager.getHeroByPlayer((Player) entity);
            if(reduction == 0.25){
                s.AddSkillExperience(Skills.Skill.Acrobatics, 10);
                h.AddEffect(PotionEffectType.SLOW, 20, 5);
                ((Player)entity).sendMessage(ChatColor.DARK_GRAY + "***Roll***");
            }
            else if(reduction == 0.5){
                s.AddSkillExperience(Skills.Skill.Acrobatics, 25);
                h.AddEffect(PotionEffectType.SLOW, 20, 5);
                ((Player)entity).sendMessage(ChatColor.DARK_GRAY + "***Graceful Roll***");
            }
            else if(reduction == 1){
                s.AddSkillExperience(Skills.Skill.Acrobatics, 50);
                h.AddEffect(PotionEffectType.SLOW, 15, 5);
                ((Player)entity).sendMessage(ChatColor.DARK_GRAY + "***Master Roll***");
            }

            double damage = event.getDamage();
            damage = damage * (1.0 - reduction);
            event.setDamage(damage);
        }
        s.AddSkillExperience(Skills.Skill.Acrobatics, (int) event.getDamage() * 2);
    }
    //endregion

    //region Building
    @EventHandler
    public void OnBuild(BlockPlaceEvent event){
        Player p = event.getPlayer();
        Block b = event.getBlock();
        if(p == null)
            return;
        else if(!Building.IsBuildingBlock(b))
            return;

        Hero h = HeroManager.getHeroByPlayer(p);
        h.getSkills().AddSkillExperience(Skills.Skill.Building, Building.BuildExpPerBlock);
    }

    @EventHandler
    public void OnCarveStairs(PlayerInteractEvent event){
        Player p = event.getPlayer();
        Block b = event.getClickedBlock();
        ItemStack item = p.getInventory().getItemInMainHand();

        if(event.getAction() != Action.RIGHT_CLICK_BLOCK)
            return;
        else if(p == null)
            return;
        else if(b == null)
            return;
        else if(item != null && !item.getType().name().toLowerCase().contains("pickaxe"))
            return;

        Hero h = HeroManager.getHeroByPlayer(p);
        Building.ConvertBlockToStair(b, p);
    }
    //endregion

    //region Digging
    @EventHandler
    public void OnDigging(BlockBreakEvent event) {
        Player player = event.getPlayer();
        if(player != null && player.getGameMode() == GameMode.SURVIVAL) {
            Hero hero = HeroManager.getHeroByPlayer(player);
            Skills skills = hero.getSkills();
            Integer digging = skills.GetSkillLevel(Skills.Skill.Digging);
            Block block = event.getBlock();

            if (Digging.IsDiggableBlock(block)) {
                int roll = random.nextInt(1000);

                if(roll < Digging.GetTreasureFindChance(skills)){
                    player.sendMessage(ChatColor.GREEN + "***TREASURE***");
                    ItemStack item = Digging.GetRandomTreasure();
                    if(player.getInventory().firstEmpty() != -1)
                        player.getInventory().addItem(item);
                    else
                        player.getWorld().dropItem(player.getLocation(), item);
                }

                skills.AddSkillExperience(Skills.Skill.Digging, 5);
            }
        }
    }
    //endregion

    //region Mining
    @EventHandler
    public void OnMining(BlockBreakEvent event){
        Player player = event.getPlayer();
        Block block = event.getBlock();

        if(player == null)
            return;
        else if(block == null)
            return;
        else if(!Mining.IsMinableBlock(block))
            return;
        else if(player.getGameMode() != GameMode.SURVIVAL)
            return;

        Hero hero = HeroManager.getHeroByPlayer(player);
        Skills skills = hero.getSkills();
        Integer mining = skills.GetSkillLevel(Skills.Skill.Mining);

        int multiplier = 1;
        int roll = random.nextInt(1000);

        //x2 chance max = 50%
        if(roll <= Mining.GetDoubleDropChance(mining)){
            player.sendMessage(ChatColor.GREEN + "***Lucky Miner***");
            multiplier = 2;

            //x3 chance max = 25%
            if(roll <= Mining.GetTripleDropChance(mining)){
                multiplier = 3;

                //x4 chance max = 12.5%
                if(roll <= Mining.GetQuadruple(mining)){
                    multiplier = 4;

                    //x5 chance max = 6.75%
                    if(roll <= Mining.GetQuintuple(mining)){
                        multiplier = 5;

                        //x6 chance max = 3.875%
                        if(roll <= Mining.GetSixtuple(mining)){
                            multiplier = 6;
                        }
                    }
                }
            }
        }

        Collection<ItemStack> drops = block.getDrops();

        //Give all the drops from a block
        for(ItemStack item : drops){
            item.setAmount(item.getAmount() * multiplier);

            if(player.getInventory().firstEmpty() == -1){
                player.getLocation().getWorld().dropItem(player.getLocation(), item);
            }
            else{
                player.getInventory().addItem(item);
            }
        }

        skills.AddSkillExperience(Skills.Skill.Mining, (int) (GetExperienceForBlock(block) * Mining.ExperienceMultiplier));

        //Damage tool since we are cancelling the event globally
        ItemStack item = player.getInventory().getItemInMainHand();
        item.setDurability((short) (item.getDurability() + 1));

        block.setType(Material.AIR);
        event.setCancelled(true);
    }

    //Player Sneaks + Right Clicks while holding pick
    @EventHandler
    public void OnTriningTrigger(PlayerInteractEvent event){
        Player p = event.getPlayer();
        Block b = event.getClickedBlock();
        ItemStack item = p.getInventory().getItemInMainHand();

        if(event.getAction() != Action.RIGHT_CLICK_BLOCK)
            return;
        else if(p == null)
            return;
        else if(!p.isSneaking())
            return;
        else if(b == null)
            return;
        else if(item != null && !item.getType().name().toLowerCase().contains("pickaxe"))
            return;

        Mining.ToggleTrining(p);
    }
    //endregion

    
}
