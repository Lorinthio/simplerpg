package me.Lorinth.SimpleRpg.listeners;

import me.Lorinth.SimpleRpg.character.Hero;
import me.Lorinth.SimpleRpg.main.HeroManager;
import org.bukkit.command.Command;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

/**
 * Created by Ben on 1/11/2017.
 */
public class HeroDataListener implements Listener {

   //This class manages loading/saving data for heroes
    //Rules
        //Load instance when player logs in
        //Save when player logs out
        //Save all players when plugin is disabled

    public boolean handleCommand(Player p, Command cmd, String commandLabel, String[] args) {
        if (commandLabel.equalsIgnoreCase("stats")) {
            Hero h = HeroManager.getHeroByPlayer(p);
            h.SendDetails(p);
            return true;
        }
        return false;
    }

    @EventHandler
    public void OnPlayerJoin(PlayerJoinEvent event){
        Player p = event.getPlayer();
        HeroManager.LoadHeroForPlayer(p);
    }

    @EventHandler
    public void OnPlayerQuit(PlayerQuitEvent event){
        Player p = event.getPlayer();
        HeroManager.SaveHeroForPlayer(p);
    }


}
