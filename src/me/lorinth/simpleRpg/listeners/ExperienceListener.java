package me.Lorinth.SimpleRpg.listeners;

import me.Lorinth.SimpleRpg.character.Hero;
import me.Lorinth.SimpleRpg.main.HeroManager;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;

/**
 * Created by Ben on 1/11/2017.
 */
public class ExperienceListener implements Listener{

    //This class handles players earning combat experience

    @EventHandler
    public void EntityDeathEvent(EntityDeathEvent event){
        LivingEntity entity = event.getEntity();
        Player killer = entity.getKiller();

        if(killer != null){
            Hero hero = HeroManager.getHeroByPlayer((Player) killer);
            int experience = calculateExperience(hero, entity);
            hero.AddExperience(experience, killer);
        }
    }

    private int calculateExperience(Hero killer, LivingEntity killed){
        if(killed instanceof Player){
            Hero dead = HeroManager.getHeroByPlayer((Player) killed);
            return calculatePvP(killer, dead);
        }
        else{
            return calculatePvE(killer, killed);
        }
    }

    private int calculatePvP(Hero killer, Hero dead){
        return 0;
    }

    private int calculatePvE(Hero killer, LivingEntity killed){
        return 0;
    }

}
