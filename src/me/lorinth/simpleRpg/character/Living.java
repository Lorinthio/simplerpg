package me.Lorinth.SimpleRpg.character;

import me.Lorinth.SimpleRpg.common.EquipmentHelper;
import me.Lorinth.SimpleRpg.enums.CombatStat;
import me.Lorinth.SimpleRpg.enums.DamageType;
import me.Lorinth.SimpleRpg.common.ISaveable;
import me.Lorinth.SimpleRpg.enums.Statistic;
import me.Lorinth.SimpleRpg.enums.Vital;
import me.Lorinth.SimpleRpg.main.HeroManager;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.*;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.projectiles.ProjectileSource;

import java.util.HashMap;

/**
 * Created by Ben on 1/10/2017.
 */
public abstract class Living extends Experienced implements ISaveable{

    protected Player player;

    private HashMap<Statistic, Integer> CoreStats = new HashMap<>();
    private HashMap<Statistic, Integer> BonusStats = new HashMap<>();
    private HashMap<Vital, Integer> CoreVital = new HashMap<>();
    private HashMap<Vital, Integer> BonusVital = new HashMap<>();
    private HashMap<CombatStat, Integer> CoreCombatStat = new HashMap<>();
    private HashMap<CombatStat, Integer> BonusCombatStat = new HashMap<>();

    private int StatPoints = 0;

    public Living(){

    }

    public void AddExperience(int exp){
        super.AddExperience(exp, this.player);
    }

    //region Equipment Access
    public int GetWeaponDamage(){
        int damage = 0;

        try{
            ItemStack item = player.getInventory().getItemInMainHand();
            ItemMeta meta = item.getItemMeta();
            if(meta.hasLore()){
                for(String line : meta.getLore()){
                    line = net.md_5.bungee.api.ChatColor.stripColor(line);
                    String[] args = line.split(":");
                    if(args.length > 1){
                        if(args[0].toLowerCase().contains("damage")){
                            damage = Integer.parseInt(args[1]);
                        }
                    }
                }
            }
            else{
                damage = EquipmentHelper.GetBaseWeaponDamage(item);
            }

            if(EquipmentHelper.IsMeleeWeapon(item))
                damage += GetStatistic(Statistic.Strength) / 20;
            if(EquipmentHelper.IsRangedWeapon(item))
                damage += GetStatistic(Statistic.Dexterity) / 20;
            if(EquipmentHelper.IsMagicWeapon(item))
                damage += GetStatistic(Statistic.Intelligence) / 20;
        }
        catch(Exception e){
            damage = 1;
        }


        return damage;
    }
    //endregion

    protected int GetStatistic(Statistic stat){
        return CoreStats.get(stat) + BonusStats.get(stat);
    }

    protected int GetVital(Vital vital){
        return CoreVital.get(vital) + BonusVital.get(vital);
    }

    protected int GetCombatStat(CombatStat stat){
        return CoreCombatStat.get(stat) + BonusCombatStat.get(stat);
    }

    protected void CreateStats(){
        //Null bonuses
        for(Statistic s : Statistic.values()){
            BonusStats.put(s, 0);
        }
        for(Vital v : Vital.values()){
            BonusVital.put(v, 0);
        }
        for(CombatStat c : CombatStat.values()){
            BonusCombatStat.put(c, 0);
        }

        for(Statistic stat : Statistic.values()){
            CoreStats.put(stat, 10);
        }

        CoreVital.put(Vital.Health, 20);
        CoreVital.put(Vital.Energy, 20);

        Update();
    }

    protected void Update(){
        //Strength
        int str = CoreStats.get(Statistic.Strength);
        CoreCombatStat.put(CombatStat.PhysicalAttack, str);

        //Constitution
        int con = CoreStats.get(Statistic.Constitution);
        int defense = (int) (con * 1.25);
        int maxHealth = (int) (20 + ((con-10.0) / 4.0));
        CoreCombatStat.put(CombatStat.PhysicalDefense, defense);
        CoreVital.put(Vital.MaxHealth, maxHealth);

        //Dexterity
        int dex = CoreStats.get(Statistic.Dexterity);
        CoreCombatStat.put(CombatStat.RangedAttack, dex);

        //Wisdom
        int wis = CoreStats.get(Statistic.Wisdom);
        int mDefense = (int) (wis * 1.25);
        int maxEnergy = (int) (20 + ((wis-10.0) / 4.0));
        CoreCombatStat.put(CombatStat.MagicDefense, mDefense);
        CoreVital.put(Vital.MaxEnergy, maxEnergy);

        //Intelligence
        int Int = CoreStats.get(Statistic.Intelligence);
        CoreCombatStat.put(CombatStat.MagicAttack, Int);

        CheckEquipment();
        UpdateVitals();
    }

    private void CheckEquipment(){

    }

    private void UpdateVitals(){
        player.setHealthScaled(true);
        player.setHealthScale(40);

        double hp = (double) GetVital(Vital.Health);
        double maxHp = (double) GetVital(Vital.MaxHealth);

        player.setMaxHealth(maxHp);
        player.setHealth(hp);

        double energy = (double) GetVital(Vital.Energy);
        double maxEnergy = (double) GetVital(Vital.MaxEnergy);
    }

    //Called when this living thing is attacked
    public void Damage(Entity entity, double damage){
        DamageType type = DamageType.Melee;

        if(entity instanceof Projectile){
            if(entity instanceof Arrow){
                type = DamageType.Ranged;
            }
            else{
                type = DamageType.Magic;
            }

            ProjectileSource e = ((Projectile) entity).getShooter();

            if(e instanceof LivingEntity)
                entity = (LivingEntity) e;
            else
                return;
        }

        if(entity instanceof Player){
            Hero hero = HeroManager.getHeroByPlayer((Player)entity);
            DamagedByHero(hero, type);
        }
        else{
            DamagedByCreature((LivingEntity) entity, damage, type);
        }
    }

    //hero is the hero attacking this hero
    private void DamagedByHero(Hero hero, DamageType type){
        double attack = 0;
        double defense = 0;

        switch(type){
            case Melee:
                attack = hero.GetCombatStat(CombatStat.PhysicalAttack);
                defense = GetCombatStat(CombatStat.PhysicalDefense);
                break;
            case Ranged:
                attack = hero.GetCombatStat(CombatStat.RangedAttack);
                defense = GetCombatStat(CombatStat.PhysicalDefense);
                break;
            case Magic:
                attack = hero.GetCombatStat(CombatStat.MagicAttack);
                defense = GetCombatStat(CombatStat.MagicDefense);
                break;
        }

        attack += 25.0;
        defense += 25.0;

        double modifier = attack / defense;
    }

    private void DamagedByCreature(LivingEntity creature, double damage, DamageType type){
        player.setHealth(player.getHealth() - damage);
    }

    //Called when we are attacking a non player
    //player v player attacks are handled by Damage()
    public void Attack(LivingEntity entity){
        int damage = this.GetWeaponDamage();
        entity.setHealth(Math.max(0, entity.getHealth() - damage));

        if(entity.getHealth() == 0){
            this.AddExperience(10);
        }
    }

    @Override
    public void Save(YamlConfiguration yml) {
        super.Save(yml);

        for(Vital vital : CoreVital.keySet()){
            yml.set("Vitals." + vital.name(), CoreVital.get(vital));
        }
        for(Statistic stat : CoreStats.keySet()){
            yml.set("Statistics." + stat.name(), CoreStats.get(stat));
        }
    }

    @Override
    public void Load(YamlConfiguration yml) {
        super.Load(yml);

        for(String key : yml.getConfigurationSection("Statistics").getKeys(false)){
            CoreStats.put(Statistic.valueOf(key), yml.getInt("Statistics." + key));
        }
        for(String key : yml.getConfigurationSection("Vitals").getKeys(false)){
            CoreVital.put(Vital.valueOf(key), yml.getInt("Vitals." + key));
        }

        //Null bonuses
        for(Statistic s : Statistic.values()){
            BonusStats.put(s, 0);
        }
        for(Vital v : Vital.values()){
            BonusVital.put(v, 0);
        }
        for(CombatStat c : CombatStat.values()){
            BonusCombatStat.put(c, 0);
        }

        Update();
    }

}
