package me.Lorinth.SimpleRpg.character;

import me.Lorinth.SimpleRpg.enums.Statistic;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

/**
 * Created by Ben on 1/10/2017.
 */
public class Hero extends Living{

    private Skills skills;
    private HeroClass heroClass = HeroClass.Adventurer;

    public enum HeroClass{
        Adventurer, Warrior, Paladin, Ranger, Mage
    }

    public Hero(Player player){
        this.player = player;
        skills = new Skills(this);
    }

    public void Create(){
        //Pass handling to HeroCreationSequence
        skills.Create();
        CreateStats();
        level = 1;
    }

    public void LevelUp(){
        this.level += 1;

        player.sendMessage(ChatColor.GOLD + "LEVEL UP!");
    }

    public void AddEffect(PotionEffectType type, int duration, int level){
        player.addPotionEffect(new PotionEffect(type, duration, level));
    }

    public Player GetPlayer(){
        return player;
    }

    public Skills getSkills(){
        return this.skills;
    }

    public void SendDetails(Player p){
        for(int i=0; i < 10; i++){
            p.sendMessage("");
        }
        p.sendMessage(ChatColor.GOLD + "Hero Details");
        p.sendMessage(ChatColor.GRAY + "Level : " + ChatColor.GOLD + level + ChatColor.DARK_GRAY + " (" + ChatColor.GOLD + experience + ChatColor.DARK_GRAY + " / " + ChatColor.GRAY + this.getNextLevel() + ChatColor.DARK_GRAY + ")");
        p.sendMessage(ChatColor.GRAY + "Class : " + GetClassName());
        p.sendMessage(ChatColor.RED + "STR" + ChatColor.GRAY + " : " + ChatColor.GOLD + GetStatistic(Statistic.Strength));
        p.sendMessage(ChatColor.GOLD + "CON" + ChatColor.GRAY + " : " + ChatColor.GOLD + GetStatistic(Statistic.Constitution));
        p.sendMessage(ChatColor.GREEN + "DEX" + ChatColor.GRAY + " : " + ChatColor.GOLD + GetStatistic(Statistic.Dexterity));
        p.sendMessage(ChatColor.BLUE + "WIS" + ChatColor.GRAY + " : " + ChatColor.GOLD + GetStatistic(Statistic.Wisdom));
        p.sendMessage(ChatColor.LIGHT_PURPLE + "INT" + ChatColor.GRAY + " : " + ChatColor.GOLD + GetStatistic(Statistic.Intelligence));
    }

    private String GetClassName(){
        switch(heroClass){
            case Adventurer:
                return ChatColor.DARK_GRAY + "Adventurer";
            case Mage:
                return ChatColor.LIGHT_PURPLE + "Mage";
            case Paladin:
                return ChatColor.GOLD + "Paladin";
            case Ranger:
                return ChatColor.GREEN + "Ranger";
            case Warrior:
                return ChatColor.RED + "Warrior";
        }
        return "";
    }

    @Override
    public void Save(YamlConfiguration yml) {
        yml.set("Class", heroClass.name());
        super.Save(yml);
        skills.Save(yml);
    }

    @Override
    public void Load(YamlConfiguration yml) {
        heroClass = HeroClass.valueOf(yml.getString("Class"));
        skills.Load(yml);
        super.Load(yml);
    }
}
