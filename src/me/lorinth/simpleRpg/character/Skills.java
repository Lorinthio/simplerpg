package me.Lorinth.SimpleRpg.character;

import me.Lorinth.SimpleRpg.skills.Building;
import me.Lorinth.SimpleRpg.skills.Digging;
import me.Lorinth.SimpleRpg.common.ISaveable;
import me.Lorinth.SimpleRpg.skills.Acrobatics;
import me.Lorinth.SimpleRpg.skills.Mining;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import java.util.HashMap;

/**
 * Created by Ben on 1/10/2017.
 */
public class Skills implements ISaveable{

    private Hero hero;
    private HashMap<Skill, Integer> SkillLevels;
    private HashMap<Skill, Integer> SkillExp;

    public enum Skill{
        Acrobatics,
        Axes,
        Bows,
        Building,
        Defense,
        Digging,
        Farming,
        Fishing,
        Mining,
        Staves,
        Swords,
        Unarmed,
        Woodcutting
    }

    public Skills(Hero hero){
        this.hero = hero;

        SkillLevels = new HashMap<>();
        SkillExp = new HashMap<>();
    }

    public void Create(){
        for(Skill skill : Skill.values()){
            SkillLevels.put(skill, 1);
            SkillExp.put(skill, 0);
        }
    }

    public Integer GetSkillLevel(Skill skill){
        if(SkillLevels.containsKey(skill))
            return SkillLevels.get(skill);

        return 1;
    }

    private Integer GetNextLevelExp(int level){
        return 200 + level*200;
    }

    public static String GetSkillNameText(Skill skill){
        switch(skill){
            case Acrobatics:
                return ChatColor.GREEN + "Acrobatics";
            case Axes:
                return ChatColor.DARK_RED + "Axes";
            case Bows:
                return ChatColor.DARK_GREEN + "Bows";
            case Building:
                return ChatColor.YELLOW + "Building";
            case Defense:
                return ChatColor.DARK_AQUA + "Defense";
            case Digging:
                return ChatColor.GOLD + "Digging";
            case Farming:
                return ChatColor.LIGHT_PURPLE + "Farming";
            case Fishing:
                return ChatColor.AQUA + "Fishing";
            case Mining:
                return ChatColor.GOLD + "Mining";
            case Staves:
                return ChatColor.DARK_PURPLE + "Staves";
            case Swords:
                return ChatColor.DARK_AQUA + "Swords";
            case Unarmed:
                return ChatColor.DARK_GRAY + "Unarmed";
            case Woodcutting:
                return ChatColor.GREEN + "Wood-cutting";
        }
        return ChatColor.RED + "NOT DEFINED";
    }

    //Used only for checking/displaying.
    //Returns safe copy
    public HashMap<Skill, Integer> GetAllSkills(){
        HashMap<Skill, Integer> skillCopy = new HashMap<>();
        for(Skill key : SkillLevels.keySet()){
            skillCopy.put(key, SkillLevels.get(key));
        }

        return skillCopy;
    }

    public void SendSkills(Player p){
        for(Skill s : Skill.values()){
            p.sendMessage(getSkillExperienceLine(s));
        }
    }

    private String getSkillExperienceLine(Skill s){
        return GetSkillNameText(s) + ChatColor.GRAY + " (" + ChatColor.GOLD + SkillLevels.get(s) + ChatColor.GRAY + ") : " + ChatColor.DARK_GRAY + "(" + ChatColor.GOLD + SkillExp.get(s)
                + ChatColor.DARK_GRAY + " / " + ChatColor.GRAY + GetNextLevelExp(SkillLevels.get(s)) + ChatColor.DARK_GRAY + ")";
    }

    public void AddSkillExperience(Skill s, int exp){
        int current = SkillExp.get(s);
        current += exp;

        int required = GetNextLevelExp(SkillLevels.get(s));
        while(current >= required){
            current -= required;

            LevelUpSkill(s);

            required = GetNextLevelExp(SkillLevels.get(s));
        }
        SkillExp.put(s, current);

    }

    private void LevelUpSkill(Skill s){
        SkillLevels.put(s, SkillLevels.get(s) + 1);
        hero.GetPlayer().sendMessage(GetSkillNameText(s) + ChatColor.GRAY + " has increased to level " + ChatColor.GOLD + SkillLevels.get(s));

        if(s == Skill.Acrobatics){
            UpdateMoveSpeed();
        }
    }

    public void SendDetailedSkillData(Skill skill, Player p){
        switch(skill){
            case Acrobatics:
                p.sendMessage("");
                p.sendMessage(getSkillExperienceLine(Skill.Acrobatics));
                Acrobatics.SendDetailedInfo(this, p);
                break;
            case Axes:
                p.sendMessage(getSkillExperienceLine(Skill.Axes));
                break;
            case Bows:
                p.sendMessage(getSkillExperienceLine(Skill.Bows));
                break;
            case Building:
                p.sendMessage("");
                p.sendMessage(getSkillExperienceLine(Skill.Building));
                Building.SendDetailedInfo(this, p);
                break;
            case Defense:
                p.sendMessage(getSkillExperienceLine(Skill.Defense));
                break;
            case Digging:
                p.sendMessage("");
                p.sendMessage(getSkillExperienceLine(Skill.Digging));
                Digging.SendDetailedInfo(this, p);
                break;
            case Farming:
                p.sendMessage(getSkillExperienceLine(Skill.Farming));
                break;
            case Fishing:
                p.sendMessage(getSkillExperienceLine(Skill.Fishing));
                break;
            case Mining:
                p.sendMessage("");
                p.sendMessage(getSkillExperienceLine(Skill.Mining));
                Mining.SendDetailedInfo(this, p);
                break;
            case Staves:
                p.sendMessage(getSkillExperienceLine(Skill.Staves));
                break;
            case Swords:
                p.sendMessage(getSkillExperienceLine(Skill.Swords));
                break;
            case Woodcutting:
                p.sendMessage(getSkillExperienceLine(Skill.Woodcutting));
                break;
        }
    }

    public void Save(YamlConfiguration yml){
        for(Skill s : SkillLevels.keySet()){
            yml.set("Skills.Levels." + s.name(), SkillLevels.get(s));
        }
        for(Skill s : SkillExp.keySet()){
            yml.set("Skills.Exp." + s.name(), SkillExp.get(s));
        }
    }

    public void Load(YamlConfiguration yml){
        for(String key : yml.getConfigurationSection("Skills.Levels").getKeys(false)){
            SkillLevels.put(Skill.valueOf(key), yml.getInt("Skills.Levels." + key));
        }
        for(String key : yml.getConfigurationSection("Skills.Exp").getKeys(false)){
            SkillExp.put(Skill.valueOf(key), yml.getInt("Skills.Exp." + key));
        }

        UpdateMoveSpeed();
    }

    private void UpdateMoveSpeed(){
        int current = SkillLevels.get(Skill.Acrobatics);
        float bonus = (float) current / (float) 5000;
        hero.GetPlayer().setWalkSpeed(0.2f + bonus);
    }

}
