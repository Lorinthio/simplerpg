package me.Lorinth.SimpleRpg.character;

import me.Lorinth.SimpleRpg.common.ISaveable;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

/**
 * Created by Ben on 1/10/2017.
 */
public abstract class Experienced implements ISaveable{

    protected int level;
    protected int experience;
    protected int totalExperience;

    protected abstract void LevelUp();

    public void AddExperience(int exp, Player player){
        this.experience += exp;
        this.totalExperience += exp;

        checkLevelUp();
    }

    private void checkLevelUp() {
        int next = getNextLevel();
        while(this.experience > next){
            this.experience -= next;
            this.LevelUp();

            next = getNextLevel();
        }
    }

    protected int getNextLevel(){
        //level 2 = 750
        //level 5 = 1500
        //level 10 = 2750
        //level 20 = 5700
        //level 30 = 9000
        //level 50 = 16800
        //level 75 = 28800
        //level 100 = 43300

        int total = 500 + (this.level-1) * 250;
        if(this.level > 10){
            total += ((this.level-5) * (this.level-5) * 2);
        }
        return total;
    }

    public void Save(YamlConfiguration yml){
        yml.set("Level", level);
        yml.set("Experience", experience);
        yml.set("TotalExperience", totalExperience);
    }

    public void Load(YamlConfiguration yml){
        level = yml.getInt("Level");
        experience = yml.getInt("Experience");
        totalExperience = yml.getInt("TotalExperience");
    }

}
