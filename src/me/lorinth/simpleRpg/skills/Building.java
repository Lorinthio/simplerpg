package me.Lorinth.SimpleRpg.skills;

import me.Lorinth.SimpleRpg.character.Skills;
import me.Lorinth.SimpleRpg.common.PlayerHelper;
import me.Lorinth.SimpleRpg.main.HeroManager;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Ben on 1/16/2017.
 */
public class Building {

    public final static int BuildExpPerBlock = 5;

    final static ArrayList<Material> buildingMaterials = new ArrayList<Material>(){{
        add(Material.COBBLE_WALL);
        add(Material.COBBLESTONE);
        add(Material.COBBLESTONE_STAIRS);
        add(Material.MOSSY_COBBLESTONE);
        add(Material.SANDSTONE_STAIRS);
        add(Material.ACACIA_STAIRS);
        add(Material.BIRCH_WOOD_STAIRS);
        add(Material.BRICK_STAIRS);
        add(Material.DARK_OAK_STAIRS);
        add(Material.JUNGLE_WOOD_STAIRS);
        add(Material.NETHER_BRICK_STAIRS);
        add(Material.QUARTZ_STAIRS);
        add(Material.QUARTZ_BLOCK);
    }};

    final static HashMap<Material, Material> stairMaterials = new HashMap<Material, Material>(){{
        put(Material.COBBLESTONE, Material.COBBLESTONE_STAIRS); //cobblestone stairs
        put(Material.SANDSTONE, Material.SANDSTONE_STAIRS); //sandstone stairs
        put(Material.RED_SANDSTONE, Material.RED_SANDSTONE_STAIRS); //sandstone stairs
        put(Material.QUARTZ_BLOCK, Material.QUARTZ_STAIRS); //quartz stairs
        put(Material.BRICK, Material.BRICK_STAIRS); //brick stairs
        put(Material.NETHER_BRICK, Material.NETHER_BRICK_STAIRS); //nether brick stairs
        put(Material.PURPUR_BLOCK, Material.PURPUR_STAIRS); //pupur stairs
        put(Material.STONE, Material.SMOOTH_STAIRS); //Smooth stone stairs
    }};

    public static boolean IsBuildingBlock(Block b){
        return buildingMaterials.contains(b.getType());
    }

    public static boolean IsStairBlock(Block b){
        return stairMaterials.keySet().contains(b.getType());
    }

    public static double IsFreePlacement(Skills s){
        return s.GetSkillLevel(Skills.Skill.Building) / 20.0;
    }

    public static boolean ConvertBlockToStair(Block b, Player p){
        if(IsStairBlock(b)){
            Skills s = HeroManager.getSkillsByPlayer(p);
            if(s.GetSkillLevel(Skills.Skill.Building) > 100){
                BlockFace face = PlayerHelper.GetPlayerDirection(p);
                face = PlayerHelper.FlipBlockFaceDirection(face); //Get the opposite facing direction

                s.AddSkillExperience(Skills.Skill.Building, 4);

                byte d = 0;

                if(face == BlockFace.WEST){
                    d = 0x3;
                }else if(face == BlockFace.EAST){
                    d = 0x2;
                }else if(face == BlockFace.NORTH){
                    d = 0x0;
                }else if(face == BlockFace.SOUTH){
                    d = 0x1;
                }

                b.setType(stairMaterials.get(b.getType()));
                b.setData(d);
                return true;
            }
        }
        return false;
    }

    public static void SendDetailedInfo(Skills skills, Player player) {
        DecimalFormat format = new DecimalFormat("%0.00");
        player.sendMessage(ChatColor.GOLD + "" + ChatColor.UNDERLINE + "Perks");
        player.sendMessage(ChatColor.GOLD + "Experienced Builder" + ChatColor.GRAY + " : " + ChatColor.DARK_AQUA + format.format(IsFreePlacement(skills) / 1000) + ChatColor.GRAY + " - Building blocks are sometimes not consumed.");
        player.sendMessage(ChatColor.GOLD + "Carve Stairs (100+)" + ChatColor.GRAY + " : Right click a block to convert it into stairs!");
    }
}
