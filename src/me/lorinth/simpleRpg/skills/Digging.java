package me.Lorinth.SimpleRpg.skills;

import me.Lorinth.SimpleRpg.character.Skills;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Ben on 1/16/2017.
 */
public class Digging {

    private static Random random = new Random();

    final static ArrayList<Material> DiggingMaterials = new ArrayList<Material>(){{
        add(Material.DIRT);
        add(Material.SAND);
        add(Material.GRAVEL);
        add(Material.SAND);
        add(Material.GRASS_PATH);
        add(Material.GRASS);
        add(Material.SOUL_SAND);
        add(Material.CLAY);
    }};

    final static ArrayList<Material> TreasureItems = new ArrayList<Material>(){{
        add(Material.GOLD_NUGGET);
        add(Material.FLINT);
        add(Material.SEEDS);
        add(Material.BEETROOT_SEEDS);
        add(Material.MELON_SEEDS);
        add(Material.PUMPKIN_SEEDS);
    }};

    public static boolean IsDiggableBlock(Block b){
        return DiggingMaterials.contains(b.getType());
    }

    public static double GetTreasureFindChance(Skills s){
        int level = s.GetSkillLevel(Skills.Skill.Digging);
        return level / 100.0;
    }

    public static ItemStack GetRandomTreasure(){
        ItemStack item = new ItemStack(TreasureItems.get(random.nextInt(TreasureItems.size())), getTreasureItemCount());
        return item;
    }

    private static int getTreasureItemCount(){
        int roll = random.nextInt(20);
        if(roll < 10)
            return 1;
        else if(roll < 14)
            return 2;
        else if(roll < 17)
            return 3;
        else if(roll < 19)
            return 4;
        else
            return 5;
    }

    public static void SendDetailedInfo(Skills skills, Player player) {
        DecimalFormat format = new DecimalFormat("%0.00");
        double treasureChance = GetTreasureFindChance(skills);
        player.sendMessage(ChatColor.GOLD + "" + ChatColor.UNDERLINE + "Perks");
        player.sendMessage(ChatColor.GOLD + "Treasure Finder" + ChatColor.GRAY + " : " + ChatColor.DARK_AQUA + format.format((treasureChance / 1000.00)) + ChatColor.GRAY + " - Chance to find trinkets in the ground..");
    }

}
