package me.Lorinth.SimpleRpg.skills;

import me.Lorinth.SimpleRpg.character.Skills;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.text.DecimalFormat;

/**
 * Created by Ben on 1/14/2017.
 */
public class Acrobatics {

    public static final int DodgeExpAward = 20;

    public static double GetDodgeChance(Skills s){
        double Acrobatics = (double) s.GetSkillLevel(Skills.Skill.Acrobatics);
        return Acrobatics / 10.0;
    }

    public static double GetRollChance(Skills s){
        double Acrobatics = (double) s.GetSkillLevel(Skills.Skill.Acrobatics);
        return Acrobatics / 2.0;
    }

    public static double GetGracefulRollChance(Skills s){
        double Acrobatics = (double) s.GetSkillLevel(Skills.Skill.Acrobatics);
        return Acrobatics / 4.0;
    }

    public static double GetMasterRollChance(Skills s){
        double Acrobatics = (double) s.GetSkillLevel(Skills.Skill.Acrobatics);
        return Acrobatics / 16.0;
    }

    public static void SendDetailedInfo(Skills skills, Player player) {
        DecimalFormat format = new DecimalFormat("%0.00");
        player.sendMessage(ChatColor.GOLD + "" + ChatColor.UNDERLINE + "Perks");
        player.sendMessage(ChatColor.GOLD + "Dodge" + ChatColor.GRAY + " : " + ChatColor.DARK_AQUA + format.format(GetDodgeChance(skills) / 1000) + ChatColor.GRAY + " - chance to avoid damage");
        player.sendMessage(ChatColor.GOLD + "Roll" + ChatColor.GRAY + " : " + ChatColor.DARK_AQUA + format.format(GetRollChance(skills) / 1000) + ChatColor.GRAY + " - chance to reduce fall damage by 25%");
        player.sendMessage(ChatColor.GOLD + "Graceful Roll" + ChatColor.GRAY + " : " + ChatColor.DARK_AQUA + format.format(GetGracefulRollChance(skills) / 1000) + ChatColor.GRAY + " - chance to reduce fall damage by 50%");
        player.sendMessage(ChatColor.GOLD + "Master Roll" + ChatColor.GRAY + " : " + ChatColor.DARK_AQUA + format.format(GetMasterRollChance(skills) / 1000) + ChatColor.GRAY + " - chance to reduce fall damage by 100%");
    }
}
