package me.Lorinth.SimpleRpg.skills;

import me.Lorinth.SimpleRpg.character.Skills;
import me.Lorinth.SimpleRpg.main.HeroManager;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

import java.text.DecimalFormat;
import java.util.ArrayList;

/**
 * Created by Ben on 1/15/2017.
 */
public class Mining {

    public final static double ExperienceMultiplier = 3.0;
    private final static ArrayList<Player> Triners = new ArrayList<>();

    final static ArrayList<Material> miningMaterials = new ArrayList<Material>(){
        {
            add(Material.SANDSTONE);
            add(Material.COBBLESTONE);
            add(Material.NETHER_BRICK);
            add(Material.STONE);
            add(Material.COAL_ORE);
            add(Material.IRON_ORE);
            add(Material.GOLD_ORE);
            add(Material.GLOWSTONE);
            add(Material.REDSTONE_ORE);
            add(Material.QUARTZ_ORE);
            add(Material.LAPIS_ORE);
            add(Material.EMERALD_ORE);
            add(Material.DIAMOND_ORE);
        }
    };

    public static void ToggleTrining(Player p){
        Skills s = HeroManager.getSkillsByPlayer(p);
        if(s.GetSkillLevel(Skills.Skill.Mining) > 100){
            if(Triners.contains(p)){
                Triners.remove(p);
                p.sendMessage(ChatColor.GRAY + "[" + Skills.GetSkillNameText(Skills.Skill.Mining) + ChatColor.GRAY + "] : " + ChatColor.DARK_AQUA + "Trining" + ChatColor.GRAY + " has been " + ChatColor.GREEN + "activated" + ChatColor.GRAY + "!");
            }
            else{
                Triners.add(p);
                p.sendMessage(ChatColor.GRAY + "[" + Skills.GetSkillNameText(Skills.Skill.Mining) + ChatColor.GRAY + "] : " + ChatColor.DARK_AQUA + "Trining" + ChatColor.GRAY + " has been " + ChatColor.RED + "deactivated" + ChatColor.GRAY + "!");
            }

        }
    }

    public static int GetExperienceForBlock(Block b){
        switch(b.getType()) {
            case SANDSTONE:
                return 1;
            case COBBLESTONE:
                return 1;
            case NETHER_BRICK:
                return 2;
            case STONE:
                return 2;
            case COAL_ORE:
                return 10;
            case GLOWSTONE:
                return 12;
            case REDSTONE_ORE:
                return 12;
            case IRON_ORE:
                return 15;
            case GOLD_ORE:
                return 20;
            case LAPIS_ORE:
                return 25;
            case DIAMOND_ORE:
                return 35;
            case EMERALD_ORE:
                return 50;
            default:
                return 1;
        }
    }

    public static boolean IsMinableBlock(Block b){
        return miningMaterials.contains(b.getType());
    }

    public static double GetDoubleDropChance(int miningLevel){
        return miningLevel / 4.00;
    }

    public static double GetTripleDropChance(int miningLevel){
        return miningLevel / 8.00;
    }

    public static double GetQuadruple(int miningLevel){
        return miningLevel / 16.00;
    }

    public static double GetQuintuple(int miningLevel){
        return miningLevel / 32.00;
    }

    public static double GetSixtuple(int miningLevel){
        return miningLevel / 64.00;
    }

    public static void SendDetailedInfo(Skills skills, Player player) {
        DecimalFormat format = new DecimalFormat("%0.00");
        int mining = skills.GetSkillLevel(Skills.Skill.Mining);
        player.sendMessage(ChatColor.GOLD + "" + ChatColor.UNDERLINE + "Perks");
        player.sendMessage(ChatColor.GOLD + "Trining" + ChatColor.GRAY + " : Use Shift + Right Click while holding a pick to toggle 3x3 or 1x1 mining");
        player.sendMessage(ChatColor.GOLD + "Vacuum" + ChatColor.GRAY + " : Blocks mined automatically go into your inventory, or are dropped at your feet.");
        player.sendMessage(ChatColor.GOLD + "Lucky Miner" + ChatColor.GRAY + " : Gives a chance to find additional ore! Drop table below");
        player.sendMessage(ChatColor.GOLD + "+ 1" + ChatColor.GRAY + " : " + ChatColor.DARK_AQUA + format.format(GetDoubleDropChance(mining) / 1000.00));
        player.sendMessage(ChatColor.GOLD + "+ 2" + ChatColor.GRAY + " : " + ChatColor.DARK_AQUA + format.format(GetTripleDropChance(mining) / 1000.00));
        player.sendMessage(ChatColor.GOLD + "+ 3" + ChatColor.GRAY + " : " + ChatColor.DARK_AQUA + format.format(GetQuadruple(mining) / 1000.00));
        player.sendMessage(ChatColor.GOLD + "+ 4" + ChatColor.GRAY + " : " + ChatColor.DARK_AQUA + format.format(GetQuintuple(mining) / 1000.00));
        player.sendMessage(ChatColor.GOLD + "+ 5" + ChatColor.GRAY + " : " + ChatColor.DARK_AQUA + format.format(GetSixtuple(mining) / 1000.00));
    }

}
