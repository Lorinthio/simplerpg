package me.Lorinth.SimpleRpg.skills;

import me.Lorinth.SimpleRpg.character.Skills;

/**
 * Created by Ben on 1/16/2017.
 */
public class Axes {
    public static double GetBonusDamage(Skills s){
        int level = s.GetSkillLevel(Skills.Skill.Axes);
        return level / 50.0;
    }

    public static double GetCrushChance(Skills s){
        int level = s.GetSkillLevel(Skills.Skill.Axes);
        return level / 75.0;
    }
}
