package me.Lorinth.SimpleRpg.common;

import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;

/**
 * Created by Ben on 1/17/2017.
 */
public class PlayerHelper {

    public static String getCardinalDirection(Player player) {
        double yaw = (player.getLocation().getYaw() - 90) % 360;
        if (yaw < 0) {
            yaw += 360.0;
        }
        return getDirection(yaw, 0.0, false);
    }

    public static String getAnyDirection(Player player){
        double yaw = (player.getLocation().getYaw() - 90) % 360;
        if (yaw < 0) {
            yaw += 360.0;
        }
        double pitch = player.getLocation().getPitch();
        return getDirection(yaw, pitch, true);
    }

    /**
     * Converts a rotation to a cardinal direction name.
     *
     * @param rot
     * @return
     */
    private static String getDirection(double rot, double pitch, boolean checkUpDown) {
        if(checkUpDown){
            if(pitch < -45)
                return "Up";
            if(pitch > 45)
                return "Down";
        }

        if (0 <= rot && rot < 45) {
            return "North";
        } else if (45 <= rot && rot < 135) {
            return "East";
        } else if (135 <= rot && rot < 225) {
            return "South";
        } else if (225 <= rot && rot < 315) {
            return "West";
        } else if (315 <= rot && rot < 360.0) {
            return "North";
        } else {
            return "North";
        }
    }

    public static BlockFace GetPlayerDirection(Player p){
        return BlockFace.valueOf(getCardinalDirection(p).toUpperCase());
    }

    public static BlockFace FlipBlockFaceDirection(BlockFace facing){
        switch(facing){
            case NORTH:
                return BlockFace.SOUTH;
            case EAST:
                return BlockFace.WEST;
            case WEST:
                return BlockFace.EAST;
            case SOUTH:
                return BlockFace.NORTH;
            default:
                return BlockFace.NORTH;
        }
    }

}
