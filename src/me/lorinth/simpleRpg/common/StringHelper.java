package me.Lorinth.SimpleRpg.common;

/**
 * Created by Ben on 1/15/2017.
 */
public class StringHelper {

    public static String MakeFirstLetterUpper(String word){
        if(word.length() > 0){
            word = word.substring(0, 1).toUpperCase() + word.substring(1, word.length());
        }
        return word;
    }

}
