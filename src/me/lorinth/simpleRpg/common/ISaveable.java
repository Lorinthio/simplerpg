package me.Lorinth.SimpleRpg.common;

import org.bukkit.configuration.file.YamlConfiguration;

/**
 * Created by Ben on 1/11/2017.
 */
public interface ISaveable {

    void Save(YamlConfiguration yml);
    void Load(YamlConfiguration yml);

}
