package me.Lorinth.SimpleRpg.common;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Ben on 1/14/2017.
 */
public class EquipmentHelper {
    private static ArrayList<Material> Swords = new ArrayList<Material>(){{
        add(Material.WOOD_SWORD);
        add(Material.STONE_SWORD);
        add(Material.IRON_SWORD);
        add(Material.GOLD_SWORD);
        add(Material.DIAMOND_SWORD);
    }};
    private static ArrayList<Material> Axes = new ArrayList<Material>(){{
        add(Material.WOOD_AXE);
        add(Material.STONE_AXE);
        add(Material.IRON_AXE);
        add(Material.GOLD_AXE);
        add(Material.DIAMOND_AXE);
    }};
    private static ArrayList<Material> Bows = new ArrayList<Material>(){{
        add(Material.BOW);
    }};
    private static ArrayList<Material> Staves = new ArrayList<Material>(){{
        add(Material.STICK);
        add(Material.BLAZE_ROD);
        add(Material.REDSTONE_TORCH_OFF);
    }};
    private static HashMap<Material, Integer> TypeDamage = new HashMap<Material, Integer>(){{
        //Swords
        put(Material.WOOD_SWORD, 4);
        put(Material.STONE_SWORD, 5);
        put(Material.IRON_SWORD, 6);
        put(Material.DIAMOND_SWORD, 7);
        put(Material.GOLD_SWORD, 5);

        //Axes
        put(Material.WOOD_AXE, 7);
        put(Material.STONE_SWORD, 8);
        put(Material.IRON_SWORD, 9);
        put(Material.DIAMOND_SWORD, 9);
        put(Material.GOLD_SWORD, 7);

        //Bow
        put(Material.BOW, 5);
    }}  ;

    public static boolean IsAxe(ItemStack item){
        return Axes.contains(item.getType());
    }

    public static boolean IsSword(ItemStack item){
        return Swords.contains(item.getType());
    }

    public static boolean IsBow(ItemStack item){
        return Bows.contains(item.getType());
    }

    public static boolean IsStaff(ItemStack item){
        return Staves.contains(item.getType());
    }

    public static boolean IsMeleeWeapon(ItemStack item){
        Material mat = item.getType();
        return Swords.contains(mat) || Axes.contains(mat);
    }

    public static boolean IsRangedWeapon(ItemStack item){
        Material mat = item.getType();
        return Bows.contains(mat);
    }

    public static boolean IsMagicWeapon(ItemStack item){
        Material mat = item.getType();
        return Staves.contains(mat);
    }

    public static int GetBaseWeaponDamage(ItemStack item){
        if(TypeDamage.containsKey(item.getType())){
            return TypeDamage.get(item.getType());
        }
        return 3;
    }

}
